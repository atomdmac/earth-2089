module.exports = {
  title: 'Daring Tales',
  description: 'Hack, Shoot, Get Paid',
  base: '/earth-2089/',
  themeConfig: {
    logo: '/logo.png',
    locales: {
      '/': {
        nav: [],
        sidebar: {
          '/': [{
            title: 'Info',
            collapsable: false,
            children: [
              '',
              'hyper-reality',
              'transportation',
              'lodging'
            ]
          }]
        }
      }
    }
  }
}
