# Transportation
![Night Bus](https://i.redd.it/rii5eot474s01.jpg)

## Public
This is often the fastest (and cheapest) way to get around.  Above ground, there are various self-driving car services that can be summoned from a variety of providers.  However, as the Conurbation has grown, above-ground transporation has become more difficult for longer distances.  This has given way to a complex, multi-level subway system.  Subway trains have been known to reach speeds of 300 mph.

## Private
Several factors have caused the widespread decline in private vehicle ownership:
 - The rapid expanse of the Conurbation
 - The spike in cost of storing a vehicle
 - The ubiquity of high-quality public transportation
 - The pervasiveness and increased quality of immersion of Hyper Reality

## Drivers
While most cars drive themselves, there are some most rare occasions when they may be piloted by a human.  This is usually only done by wealthy enthusiasts, motor sports athletes or criminals.  Users generally interface with the vehicles using their TAP.
