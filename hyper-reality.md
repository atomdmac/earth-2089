# Hyper Reality
As the Internet grew in size and complexity, the way people interacted with it began to evolve.  As the "Internet of Things" expanded to include more and more common objects and as the devices that people used to interface with them became more integrated and immersive, a new term entered the global lexicon: "Hyper Reality".  This was used to describe the increasingly common experience of being able to interact with objects in the connected world through a new and (thanks to the earnest industriousness of the world's tech corporations) widely available interface commonly known as the TAP.

## The TAP
The Tendril Access Processor, introduced in the mid-21st century, was the world's first widely accessible neural interface. Initially, the TAP was a means for helping humans keep pace with jobs requiring very high cognitive performance.  Due to it's obvious applications in entertainment and communication (as well as it's low production cost), it quickly became available to the population at large.

### Installation
TAP installation requires minor outpatient surgery.  A "seed" is installed in the brain stem of the user.  This includes a small data port that can be used for firmware upgrades or for transference of highly sensitive information (though the later is less common among the general population).

Over the course of the following weeks, the seed will spread throughout the users brain, connecting to the necessary neurons.  During this "installation period", the TAP is disabled.  Premature use of a TAP during installation can cause severe negative side-effects such as acute psychosis, hallucinations, violent behavior and brain death.

At the end of this period, users should return to the installation clinic for an initial safety check, TAP calibration and introductory training.  This follow-up visit is legally required in all developed countries.

### User Experience
Once the user's TAP has been activated, an Augmented Reality Layer becomes available.  Users can see (and often interact with) objects and interfaces that are visually overlaid onto to the real world (analogous to holograms or "Heads-Up Displays" in video games).

Due to the inherently invasive nature of the TAP, users can manually disable their TAP to avoid malicious or unwelcome content.  However, since the TAP is so intertwined with the user's brain, power-cycling the TAP takes several minutes to prevent possible neural damage or side-effects.

## Networks
As was the case with the Internet before it, Hyper Reality is derived from the connection of many distinct networks.  Below are a few of the more well-known types of networks that can be explored, probed for information or otherwise manipulated.

### Military Networks
Highly restricted and high-risk to attempt to access authorized.  If you can gain access with brute force, stealth or the right connections, these networks can contain extremely valuable information.  But you didn't read that here.

### Social Feeds
Curated, usually prerecorded content, usually by individual people.  Many popular content makers make a living from their videos and live streams.

### Cyber Cafes
Hyper Reality chat rooms modeled after brick-and-mortar bars and clubs in days of yore.  Unlike many other facets of Hyper Realtiy, these exist exclusively in VR and have no physical components.

### Corporate News Feeds
Analogous to network news, these are the state-sanctioned news sources.  They are often used as a mouth-piece for governments (and presumably, for their corporate benefactors).
